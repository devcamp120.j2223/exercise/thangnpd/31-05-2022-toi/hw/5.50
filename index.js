const express = require('express');
const app = express();
const port = 8000;
const {router, Company} = require('./app/routes/companyRoutes');

app.use('/companies', router);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
})
