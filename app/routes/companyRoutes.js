const express = require('express');
// tạo router
const router = express.Router();
const companyLists = [
  {
    id: 1,
    company: "Alfreds Futterkiste",
    contact: "Maria Anders",
    country: "Germany"
  },
  {
    id: 2,
    company: "Centro comercial Moctezuma",
    contact: "Francisco Chang",
    country: "Mexico"
  },
  {
    id: 3,
    company: "Ernst Handel",
    contact: "Roland Mendel",
    country: "Austria"
  },
  {
    id: 4,
    company: "Island Trading",
    contact: "Helen Bennett",
    country: "UK"
  },
  {
    id: 5,
    company: "Laughing Bacchus Winecellars",
    contact: "Yoshi Tannamuri",
    country: "Canada"
  },
  {
    id: 6,
    company: "Magazzini Alimentari Riuniti",
    contact: "Giovanni Rovelli",
    country: "Italy"
  }
]

let companyList = [];

//khai báo class Company với các thuộc tính
class Company {
  constructor(id, company, contact, country) {
    this.id = id;
    this.company = company;
    this.contact = contact;
    this.country = country;
  }
}

//khởi tạo 6 thực thể company nhờ dữ liệu đã cho
for(let i = 0; i < companyLists.length; i++) {
  companyList.push(new Company(companyLists[i].id, companyLists[i].company, companyLists[i].contact, companyLists[i].country));
}

router.post('/',(req, res, next) => {
  console.log(`Req params ${req.params}`);
  console.log(`Req URL ${req.originalUrl}`);
  console.log(`Req method ${req.method}`);
});

router.get('/:id',(req, res, next) => {
  console.log(companyList[(req.params.id - 1)]);
  if(req.params.id === ''){
    next('route');
  } else {
    res.status(200).json({
      ...companyList[(req.params.id - 1)]
    })
  }
});

//Tại API router get all company (“/companies”), response trả về danh sách các thực thể đã được khai báo
router.get('/',(req, res, next) => {
  res.status(200).json({
    companyList
  })
});

router.put('/:id',(req, res, next) => {
  console.log(`Req params ${req.params.id}`);
  console.log(`Req URL ${req.originalUrl}`);
  console.log(`Req method ${req.method}`);
});

router.delete('/:id',(req, res, next) => {
  console.log(`Req params ${req.params.id}`);
  console.log(`Req URL ${req.originalUrl}`);
  console.log(`Req method ${req.method}`);
});

module.exports = {router, Company};